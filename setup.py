from setuptools import setup, find_packages

setup(
    name='pywallet-example',
    version='0.1',
    packages=find_packages(exclude=['tests*']),
    license='none',
    description='An example python package using wallet that simulates users adding or spending money',
    long_description=open('README.md').read(),
    install_requires=[],
    url='https://gitlab.com/scheidec/pywallet-example',
    author='Caleb Scheidel',
    author_email='caleb@methodsconsultants.com'
)