This is an example python package used during an internal pedagogy presentation at Methods Consultants.

To install, clone the repository:

```
git clone https://gitlab.com/scheidec/pywallet-example
```

and run the `setup.py` file from that directory:

```
python setup.py install
```
